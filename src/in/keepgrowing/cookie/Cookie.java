package in.keepgrowing.cookie;

import in.keepgrowing.cache.Cacheable;

public class Cookie implements Cacheable {

    private Long id;
    private String name;

    public Cookie(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Cookie{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
