package in.keepgrowing.cookie;

import in.keepgrowing.cache.CacheableRepository;

import java.util.Arrays;
import java.util.List;

public class CookieRepository implements CacheableRepository<Cookie> {

    private List<Cookie> cookies = Arrays.asList(
            new Cookie(1L, "Oreo"),
            new Cookie(2L, "Chocolate cookie"),
            new Cookie(3L, "Oatmeal cookie"),
            new Cookie(4L, "Almond cookie")
    );

    @Override
    public Cookie findById(Long id) {
        System.out.println("Retrieving cookie from DB (id=" + id + ").");
        return cookies.stream()
                .filter(cookie -> cookie.getId().equals(id))
                .findFirst()
                .orElse(null);
    }
}
