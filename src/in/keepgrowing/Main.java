package in.keepgrowing;

import in.keepgrowing.cache.Cache;
import in.keepgrowing.cookie.Cookie;
import in.keepgrowing.cookie.CookieRepository;

import java.time.Duration;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Cache<Cookie> cookieCache = new Cache<>(new CookieRepository(), Duration.ofSeconds(3));
        cookieCache.findById(1L);
        cookieCache.findById(1L);
        System.out.println("Sleeping");
        Thread.sleep(3000);
        cookieCache.findById(1L);
        System.out.println("Clearing cache");
        cookieCache.clear();
        cookieCache.findById(1L);
        cookieCache.findById(1L);
    }
}
