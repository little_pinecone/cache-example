package in.keepgrowing.cache;

public interface Cacheable {

    Long getId();
}
