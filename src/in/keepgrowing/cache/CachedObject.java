package in.keepgrowing.cache;

import java.time.LocalDateTime;

public class CachedObject<DATA_TYPE extends Cacheable> {

    private DATA_TYPE object;
    private LocalDateTime lastRetrievedAt;

    public CachedObject(DATA_TYPE object) {
        this.object = object;
        this.lastRetrievedAt = LocalDateTime.now();
    }

    public DATA_TYPE getObject() {
        return object;
    }

    public LocalDateTime getLastRetrievedAt() {
        return lastRetrievedAt;
    }
}
