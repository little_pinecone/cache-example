package in.keepgrowing.cache;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

public class Cache<DATA_TYPE extends Cacheable> implements CacheableRepository {

    private final Map<Long, CachedObject<DATA_TYPE>> cachedObjects;
    private final CacheableRepository<DATA_TYPE> repository;
    private final Duration maxLife;

    public Cache(CacheableRepository<DATA_TYPE> repository, Duration maxLife) {
        this.cachedObjects = new HashMap<>();
        this.repository = repository;
        this.maxLife = maxLife;
    }

    public DATA_TYPE findById(Long id) {
        if(cachedObjects.containsKey(id)) {
            CachedObject<DATA_TYPE> cachedObject = cachedObjects.get(id);
            if(notExpired(cachedObject)) {
                System.out.println("Retrieving cookie from Cache (id=" + id + ").");
                return cachedObject.getObject();
            }
        }
        return retrieveAndCache(id);
    }

    private boolean notExpired(CachedObject<DATA_TYPE> cachedObject) {
        Duration life = Duration.between(cachedObject.getLastRetrievedAt(), LocalDateTime.now());
        return life.compareTo(maxLife) < 0;
    }

    private DATA_TYPE retrieveAndCache(Long id) {
        DATA_TYPE object = repository.findById(id);
        cachedObjects.put(id, new CachedObject<>(object));
        return object;
    }

    public void clear() {
        cachedObjects.clear();
    }
}
