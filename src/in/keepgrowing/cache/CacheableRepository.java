package in.keepgrowing.cache;

public interface CacheableRepository<DATA_TYPE extends Cacheable> {

    DATA_TYPE findById(Long id);
}
